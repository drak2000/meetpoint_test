package com.my.meetpoint.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Account {
	public int social_id;
	public String access_token;
    public long user_id;
    
    public void save(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor=prefs.edit();
        editor.putInt("social_id", social_id);
        editor.putString("access_token", access_token);
        editor.putLong("user_id", user_id);
        editor.commit();
    }
    
    public void restore(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        social_id = prefs.getInt("social_id", 0);
        user_id=prefs.getLong("user_id", 0);
        access_token=prefs.getString("access_token", null);
    }
}
