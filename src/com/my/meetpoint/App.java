package com.my.meetpoint;








import android.app.Application;

import com.my.meetpoint.utils.TypefaceManager;

public class App extends Application {
	@Override  
	public void onCreate()
	{
		super.onCreate();
		TypefaceManager.initialize(this, R.xml.fonts);
	}
}
