package com.my.meetpoint;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.my.meetpoint.service.Account;
import com.my.meetpoint.utils.Constants;
import com.perm.kate.api.Api;

public class IntoActivity extends Activity implements OnClickListener {
	private int social_id = 0;
	private final int REQUEST_LOGIN=1;
	Account mAccount;
	@Override
	public void onCreate(Bundle savedInstanceState){
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);	// Removes title bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN);	// Removes notification
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_into);
		mAccount = new Account();
		initButton();
	}
	private void initButton(){
		ImageView iVk = (ImageView)findViewById(R.id.image_vk);
		ImageView iInstagram = (ImageView)findViewById(R.id.image_instagram);
		ImageView iFacebook = (ImageView)findViewById(R.id.image_facebook);
		ImageView iGoogle = (ImageView)findViewById(R.id.image_google);
		iVk.setOnClickListener(this);
		iInstagram.setOnClickListener(this);
		iFacebook.setOnClickListener(this);
		iGoogle.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.image_vk:
			social_id = 1;
			break;
		case R.id.image_instagram:
			social_id = 2;
			break;
		case R.id.image_facebook:
			social_id = 3;
			break;
		case R.id.image_google:
			social_id = 4;
			break;
		}
		Intent intent = new Intent(IntoActivity.this, AuthorizationActivity.class);
		intent.putExtra(AuthorizationActivity.ID, social_id);
		startActivityForResult(intent, REQUEST_LOGIN);
	}
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOGIN) {
            if (resultCode == RESULT_OK) {
                //авторизовались успешно 
            	mAccount.social_id=data.getIntExtra("social_id", 0);
            	mAccount.user_id=data.getLongExtra("user_id", 0);
                mAccount.access_token=data.getStringExtra("token");
                mAccount.save(IntoActivity.this);
                Api api = new Api(mAccount.access_token, Constants.VK_ID);
                //showButtons();
            }
        }
    }
}
