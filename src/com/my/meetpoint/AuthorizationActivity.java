package com.my.meetpoint;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.my.meetpoint.utils.Constants;
import com.perm.kate.api.Auth;

public class AuthorizationActivity extends Activity {
	private static final String TAG = "LoginActivity";
	public static final String ID = "social_id";
	WebView webview;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth);
		Bundle data = getIntent().getExtras();
	    if (data != null ) {
	    	if (data.getInt(ID)>0){
	    		loadWeb();
	    	}
	    }
	}
	private void loadWeb(){
		webview = (WebView) findViewById(R.id.auth_web);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.clearCache(true);
        
        //����� �������� ����������� �� ��������� �������� ��������
        webview.setWebViewClient(new VkontakteWebViewClient());
                
        //otherwise CookieManager will fall with java.lang.IllegalStateException: CookieSyncManager::createInstance() needs to be called before CookieSyncManager::getInstance()
        CookieSyncManager.createInstance(this);
        
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        
        String url = Auth.getUrl(Constants.VK_ID, Auth.getSettings());
        webview.loadUrl(url);
	}
	class VkontakteWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            parseUrl(url);
        }
    }
	private String url(int Id){
		String url = null;
		return url;
	}
	private void parseUrl(String url) {
        try {
            if(url==null)
                return;
            Log.i(TAG, "url="+url);
            if(url.startsWith(Auth.redirect_url))
            {
                if(!url.contains("error=")){
                    String[] auth= Auth.parseRedirectUrl(url);
                    Intent intent=new Intent();
                    intent.putExtra("social_id", ID);
                    intent.putExtra("user_id", Long.parseLong(auth[1]));
                    intent.putExtra("token", auth[0]);
                    setResult(Activity.RESULT_OK, intent);
                }
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
